<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenerimaBarangRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
		$rules = [];

		// update rules when do an update
		if ($this->isUpdateMode()) {}

        return $rules;
    }

    /**
     * check if formData has id
     *
     * @return bool
     */
    public function isUpdateMode(): bool
    {
        return $this->segment(3) === 'update';
    }
}
