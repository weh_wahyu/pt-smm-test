<?php

namespace App\Http\Controllers;

use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests\BarangRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\BarangRepository;
use Illuminate\Validation\ValidationException;


class BarangController extends Controller
{
	/**
	 * @var BarangRepository
	 */
	protected $repository;

	/**
	 * @var string
	 */
	protected $entity;

	public function __construct(
		BarangRepository $repository
	) {
		$this->entity = 'Barang';
		$this->repository = $repository;
	}

	/**
	 * get all data
	 *
	 * @return Json
	 */
	public function index(Request $request)
	{
		try {
			$data = $this->repository->getAll($request);

			return success(
				$data = $data,
				$message = ''
			);
		} catch (Exception $e){
			return fail(
				$message = $e->getMessage()
			);
		}
	}

	/**
	 * getList data with pagination
	 *
	 * @return Json
	 */
	public function getList(Request $request)
	{
		try {
			$limit = $request->input('per_page', 10);
			$data = $this->repository->paginate($limit, $columns = ['*']);

			return success(
				$data = $data,
				$message = ''
			);
		} catch (Exception $e){
			return fail(
				$message = $e->getMessage()
			);
		}
	}

	/**
	 * get specific data by id
	 *
	 * @param mixed $id
	 * @return void
	 */
	public function get(Request $request, $id)
	{
		try {
			$data = $this->repository->getById($request, $id);

			return success(
				$data = $data,
				$message = ''
			);
		} catch (Exception $e){
			return fail(
				$message = $e->getMessage()
			);
		}
	}

	/**
	 * store
	 *
	 * @param  mixed $request
	 * @param  mixed $id
	 * @return void
	 */
	public function store(BarangRequest $request, $id = null)
	{
		try {
        	DB::beginTransaction();
			$data = $this->repository->store(
				$request,
				$id
			);
			DB::commit();

			return success(
				$data = $data,
				$message = getMessage(
					($id)
						? 'update'
						: 'save'
					, $this->entity
				)
			);
		} catch (Exception $e){
			DB::rollback();
			return fail(
				$message = $e->getMessage()
			);
		}
	}

	/**
	 * remove
	 *
	 * @param  mixed $id
	 * @return void
	 */
	public function remove($id)
	{
		try {
			DB::beginTransaction();
			$data = $this->repository->remove($id);
			DB::commit();

			return success(
				$data = $data,
				$message = getMessage('delete', $this->entity)
			);
		} catch (Exception $e){
			DB::rollback();
			return fail(
				$message = $e->getMessage()
			);
		}
	}
}
