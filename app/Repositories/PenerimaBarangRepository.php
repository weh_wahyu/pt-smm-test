<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PenerimaBarangRepository.
 *
 * @package namespace App\Repositories;
 */
interface PenerimaBarangRepository extends RepositoryInterface
{
    //
}
