<?php

namespace App\Repositories;

use App\Models\Barang;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Repositories\PermintaanBarangRepository;
use App\Models\PermintaanBarang;
use App\Validators\PermintaanBarangValidator;
use Exception;

/**
 * Class PermintaanBarangRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PermintaanBarangRepositoryEloquent extends Repository implements PermintaanBarangRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PermintaanBarang::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

	public function findIfExist($id, $columns = ['*'])
	{
		try {
			$this->isExist($id);
			return parent::find($id, $columns);
		} catch (Exception $e){
            throw new Exception($e->getMessage());
        }
	}

	/**
	 * getAll
	 *
	 * @param  mixed $request
	 * @return void
	 */
	public function getAll(Request $request)
	{
		return $this->model
			->with([])
			->orderBy('created_at', 'desc')
			->get();
	}

	/**
	 * getById
	 *
	 * @param  mixed $request
	 * @param  mixed $id
	 * @return void
	 */
	public function getById(Request $request, int $id)
	{
		return $this->model
			->with([
			    'permintaanBarangItems'
            ])
			->find($id);
	}

    /**
     * store
     *
     * @param mixed $request
     * @param mixed $id
     * @return void
     * @throws Exception
     */
	public function store(Request $request, $id = null)
	{
		try {
		    $payloadData = [
                'penerima_barang_id' => $request->penerima_barang_id,
                'total_barang' => count($request->permintaan_barang_items),
                'request_date' => $request->request_date,
                'is_approved' => true,
            ];

            // compile permintaan barang items
            $permintaanBarangItems = [];
            foreach ($request->permintaan_barang_items as $barangItem) {
                $permintaanBarangItems[$barangItem['barang_id']] = [
                    'stock' => $barangItem['stock_requested'],
                    'notes' => $barangItem['notes']
                ];

                // update stock barang
                $barang = Barang::where('id', $barangItem['barang_id'])->first();
                $barang->stock -= $barangItem['stock_requested'];
                $barang->save();
            }

			// if id is existing, then do update
			if (!empty($id)) {
				$data = $this->isExist($id);
				parent::update($payloadData, $id);
			} else {
				$data = parent::create($payloadData);
			}

            // sync data
            $data->permintaanBarangItems()->sync($permintaanBarangItems);

			return $data;
		} catch (Exception $e){
            throw new Exception($e->getMessage());
        }
	}

	/**
	 * remove
	 *
	 * @param  mixed $id
	 * @param  mixed $columns
	 * @return void
	 */
	public function remove($id, $columns = ['*'])
	{
		try {
			$this->isExist($id);
			return parent::delete($id);
		} catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
	}
}
