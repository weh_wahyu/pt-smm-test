<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermintaanBarangItemsRepository.
 *
 * @package namespace App\Repositories;
 */
interface PermintaanBarangItemsRepository extends RepositoryInterface
{
    //
}
