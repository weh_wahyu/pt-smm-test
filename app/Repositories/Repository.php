<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Http\Request;
use Exception;

/**
 * Class Repository.
 *
 * @package namespace App\Repositories;
 */
abstract class Repository extends BaseRepository
{
    /**
     * isExist
     *
     * @param string $id
     * @throws Exception
     */
	public function isExist(string $id)
	{
	    $data = $this->model->where('id', $id)->first();
        if (!$data) {
            throw new Exception('Data does not exist!');
        } else {
            return $data;
        }
	}
}
