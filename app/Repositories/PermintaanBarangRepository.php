<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermintaanBarangRepository.
 *
 * @package namespace App\Repositories;
 */
interface PermintaanBarangRepository extends RepositoryInterface
{
    //
}
