<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BarangRepository.
 *
 * @package namespace App\Repositories;
 */
interface BarangRepository extends RepositoryInterface
{
    //
}
