<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Request;
use App\Repositories\BarangRepository;
use App\Models\Barang;
use App\Validators\BarangValidator;
use Exception;

/**
 * Class BarangRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BarangRepositoryEloquent extends Repository implements BarangRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Barang::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

	public function findIfExist($id, $columns = ['*'])
	{
		try {
			$this->isExist($id);
			return parent::find($id, $columns);
		} catch (Exception $e){
            throw new Exception($e->getMessage());
        }
	}

	/**
	 * getAll
	 *
	 * @param  mixed $request
	 * @return void
	 */
	public function getAll(Request $request)
	{
		return $this->model
			->with([])
			->get();
	}

	/**
	 * getById
	 *
	 * @param  mixed $request
	 * @param  mixed $id
	 * @return void
	 */
	public function getById(Request $request, int $id)
	{
		return $this->model
			->with([])
			->find($id);
	}

	/**
	 * store
	 *
	 * @param  mixed $request
	 * @param  mixed $id
	 * @return void
	 */
	public function store(Request $request, $id = null)
	{
		try {
			// if id is existing, then do update
			if (!empty($id)) {
				$this->isExist($id);
				$data = parent::update($request->all(), $id);
			} else {
				$data = parent::create($request->all());
			}

			return $data;
		} catch (Exception $e){
            throw new Exception($e->getMessage());
        }
	}

	/**
	 * remove
	 *
	 * @param  mixed $id
	 * @param  mixed $columns
	 * @return void
	 */
	public function remove($id, $columns = ['*'])
	{
		try {
			$this->isExist($id);
			return parent::delete($id);
		} catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
	}
}
