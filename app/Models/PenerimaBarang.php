<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nik
 * @property string $name
 * @property string $department
 * @property string $description
 * @property int $is_active
 * @property string $deleted_at
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property PermintaanBarang[] $permintaanBarangs
 */
class PenerimaBarang extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'penerima_barang';

    /**
     * @var array
     */
    protected $fillable = ['nik', 'name', 'department', 'description', 'is_active', 'deleted_at', 'created_at', 'created_by', 'updated_at', 'updated_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permintaanBarangs()
    {
        return $this->hasMany('App\Models\PermintaanBarang', 'penerima_barang_id');
    }
}
