<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $penerima_barang_id
 * @property int $total_barang
 * @property string $request_date
 * @property int $is_approved
 * @property string $deleted_at
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property PenerimaBarang $penerimaBarang
 * @property PermintaanBarangItem[] $permintaanBarangItems
 */
class PermintaanBarang extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permintaan_barang';

    /**
     * @var array
     */
    protected $fillable = ['penerima_barang_id', 'total_barang', 'request_date', 'is_approved', 'deleted_at', 'created_at', 'created_by', 'updated_at', 'updated_by'];

    protected  $appends = ['nama_penerima'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function penerimaBarang()
    {
        return $this->belongsTo('App\Models\PenerimaBarang', 'penerima_barang_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permintaanBarangItems()
    {
        return $this->belongsToMany(
            'App\Models\Barang',
            'App\Models\PermintaanBarangItems',
            'permintaan_barang_id',
            'barang_id'
        )->withPivot(
            'stock',
            'notes',
        );
    }

    /**
     * @return string
     */
    public function getNamaPenerimaAttribute(): string
    {
        return $this->penerimaBarang->name;
    }
}
