<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $location
 * @property int $stock
 * @property string $description
 * @property int $is_active
 * @property string $status
 * @property string $deleted_at
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property PermintaanBarangItem[] $permintaanBarangItems
 */
class Barang extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'barang';

    /**
     * @var array
     */
    protected $fillable = ['name', 'location', 'stock', 'description', 'is_active', 'status', 'deleted_at', 'created_at', 'created_by', 'updated_at', 'updated_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permintaanBarangItems()
    {
        return $this->hasMany('App\Models\PermintaanBarangItems');
    }
}
