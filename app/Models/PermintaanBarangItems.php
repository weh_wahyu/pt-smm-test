<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $permintaan_barang_id
 * @property int $barang_id
 * @property int $stock
 * @property string $notes
 * @property Barang $barang
 * @property PermintaanBarang $permintaanBarang
 */
class PermintaanBarangItems extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['permintaan_barang_id', 'barang_id', 'stock', 'notes'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function barang()
    {
        return $this->belongsTo('App\Models\Barang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function permintaanBarang()
    {
        return $this->belongsTo('App\Models\PermintaanBarang');
    }
}
