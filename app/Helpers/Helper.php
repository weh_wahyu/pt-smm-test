<?php

if (! function_exists('success')) {

    /**
     * @param array $data
     * @param string $message
     * @param int $code
     * @param array $headers
     * @param int $httpStatusCode
     * @return \Illuminate\Http\JsonResponse
     */
    function success($data = [], string $message = '', int $code = 200, array $headers = [], int $httpStatusCode = 200)
    {
        return App\Helpers\JsonResponse::response(
            $data, $message, $code, $headers, $httpStatusCode
        );
    }
}

if (! function_exists('fail')) {

    /**
     * @param string $message
     * @param int $code
     * @param array $data
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    function fail(string $message = 'fail.', int $code = 500, $data = [], array $headers = [], int $httpStatusCode = 200)
    {
        return App\Helpers\JsonResponse::response(
            $data, $message, $code, $headers, $httpStatusCode
        );
    }
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('app_path')) {
    /**
     * Get the path to the application folder.
     *
     * @param  string $path
     * @return string
     */
    function app_path($path = '')
    {
        return app('path') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('getMessage')) {
    /**
     * Get the message with specific localization.
     *
     * @param  string $type
     * @param  string $entity
     * @return string
     */
    function getMessage(
		string $action,
		string $entity = ""
	) {
        return trans(
			"messages.{$action}",
			['name' => $entity]
		);
    }
}

if (!function_exists('formatCurrency')) {
	function formatCurrency($value, $decimalVal = 2){
		return number_format(
			sprintf(
				'%0.2f',
				preg_replace("/[^0-9.]/", "", $value)
			),
			$decimalVal
		);
	}
}