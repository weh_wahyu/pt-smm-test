<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\PenerimaBarangController;
use App\Http\Controllers\PermintaanBarangController;
use App\Http\Controllers\Settings\PasswordController;
use App\Http\Controllers\Settings\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [LoginController::class, 'logout']);

    Route::get('user', [UserController::class, 'current']);

    Route::patch('settings/profile', [ProfileController::class, 'update']);
    Route::patch('settings/password', [PasswordController::class, 'update']);

    Route::group(['prefix' => '/barang'], function() {
        Route::get('/', [BarangController::class, 'index']);
        Route::get('/table', [BarangController::class, 'getList']);
        Route::get('/detail/{id}', [BarangController::class, 'get']);
        Route::post('/save', [BarangController::class, 'store']);
        Route::put('/update/{id}', [BarangController::class, 'store']);
        Route::delete('/{id}', [BarangController::class, 'remove']);
    });

    Route::group(['prefix' => '/penerima-barang'], function() {
        Route::get('/', [PenerimaBarangController::class, 'index']);
        Route::get('/table', [PenerimaBarangController::class, 'getList']);
        Route::get('/detail/{id}', [PenerimaBarangController::class, 'get']);
        Route::post('/save', [PenerimaBarangController::class, 'store']);
        Route::put('/update/{id}', [PenerimaBarangController::class, 'store']);
        Route::delete('/{id}', [PenerimaBarangController::class, 'remove']);
    });

    Route::group(['prefix' => '/permintaan-barang'], function() {
        Route::get('/', [PermintaanBarangController::class, 'index']);
        Route::get('/table', [PermintaanBarangController::class, 'getList']);
        Route::get('/detail/{id}', [PermintaanBarangController::class, 'get']);
        Route::post('/save', [PermintaanBarangController::class, 'store']);
        Route::put('/update/{id}', [PermintaanBarangController::class, 'store']);
        Route::delete('/{id}', [PermintaanBarangController::class, 'remove']);
    });
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [RegisterController::class, 'register']);

    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::post('email/verify/{user}', [VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend']);

    Route::post('oauth/{driver}', [OAuthController::class, 'redirect']);
    Route::get('oauth/{driver}/callback', [OAuthController::class, 'handleCallback'])->name('oauth.callback');
});
