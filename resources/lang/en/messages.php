<?php

return [
	'save' => ':name data successfully saved.',
	'update' => ':name data successfully updated.',
	'delete' => ':name data successfully deleted.',

	'login' => 'Login successful.',
	'logout' => 'Logout successful.',
	'activate_account' => 'Please check your email to activate your account.',
	'suspended' => 'Your account is suspended, please contact admin to activate it.',
	'unlock' => 'Account suspension has been removed.',
];