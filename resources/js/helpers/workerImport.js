// workerImport.js
// this function is to run the generate pdf command in a different thread,
// so the UI will not be locked during the process

import axios from "axios";
import appInfo from "~/app-info";

const doJob = {
  async sync(token, id, callback) {
    try {
      let config = {
        headers: {
          Authorization: token,
        },
      };

      const res = await axios.get(process.env.VUE_APP_API_URL + appInfo.api.importData.checkStatus + "/" + id, config);

      if (res.data.code === 500) {
        throw res.data.msg;
      }

      callback(res.data);
    } catch (error) {
      console.log("sync data:", error);
    }
  },
};

self.onmessage = (e) => {
  var data = e.data;
  var payload = data.payload;
  var id = data.id;

  // Perform generate files
  // We can trigger any mutations from here!
  // self.postMessage({ type: "SET_PROGRESS", payload: false });

  doJob.sync(payload, id, (res) => {
    if (!res.data.is_finished) {
      let params = {id: id, token: payload, is_finished: res.data.is_finished}
      self.postMessage({ type: "SET_PROGRESS", payload: params });
    } else {
      let params = {is_finished: true}
      self.postMessage({ type: data.type, payload: res });
      self.postMessage({ type: "SET_PROGRESS", payload: params });
    }
  });
};
