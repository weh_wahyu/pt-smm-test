import appInfo from "~/app-info";
import axios from "axios";
import router from "~/router";

const emptyData = {
  uraian: "",
  kode_barang: "",
  tahun_perolehan: "",
  nilai_perolehan: "",
  jumlah: "",
  jenis: "",
};

var validationRules = {};

export default {
  getEmptyData() {
    return emptyData;
  },
  getValidationRule() {
    return validationRules;
  },
};
