const emptyData = {
  name: undefined,
  location: undefined,
  stock: undefined,
  is_active: false,
  description: undefined
}

var validationRules = {}

export default {
  getEmptyData () {
    return emptyData
  },
  getValidationRule () {
    return validationRules
  },
};
