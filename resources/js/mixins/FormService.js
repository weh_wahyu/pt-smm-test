import axios from 'axios'

class serviceForm {
  vue
  helper
  id

  constructor(vue, helper, id = null) {
    this.vue = vue
    this.helper = helper
    this.id = id
  }

  get initialFormData() {
    return async () => {
      let formData

      var id = this.vue.$route.params.id
      if (this.id) {
        id = this.id
      }

      if (id) {
        const res = await axios.get(`${this.vue.apiGetById}/${id}`)
        formData = res.data.data
      } else {
        formData = Object.assign({}, this.helper.getEmptyData())
      }

      return formData
    }
  }

  async submit(formData, redirect = true) {
      if (this.vue.$route.params.id || this.id) {
        await axios
          .put(`${this.vue.apiPut}/${this.id}`, formData)
          .then((response) => {
						if (response.data.code == 500) {
							throw response.data.msg
						}

            if (redirect) {
              this.vue.$router.go(-1)
            }
          })
          .catch((error) => {
            throw error
          })
      } else {
        await axios
          .post(`${this.vue.apiPost}`, formData)
          .then((response) => {
						if (response.data.code == 500) {
							throw response.data.msg
						}

            this.vue.$router.go(-1)
          })
          .catch((error) => {
            throw error
          })
      }
  }

  async submitWithFile(formData, redirect = true) {
      if (this.vue.$route.params.id || this.id) {
        await axios
          .post(
						`${this.vue.apiPut}/${this.id}?_method=PUT`,
						formData,
						{
              headers: {
                "Content-Type": "multipart/form-data",
              },
            }
					)
          .then((response) => {
						if (response.data.code == 500) {
							throw response.data.msg
						}

            if (redirect) {
              this.vue.$router.go(-1)
            }
          })
          .catch((error) => {
            throw error
          })
      } else {
        await axios
          .post(
						`${this.vue.apiPost}`,
						formData,
						{
              headers: {
                "Content-Type": "multipart/form-data",
              },
            }
					)
          .then((response) => {
						if (response.data.code == 500) {
							throw response.data.msg
						}

            if (redirect) {
              this.vue.$router.go(-1)
            }
          })
          .catch((error) => {
            throw error
          })
      }
  }

  async remove(id) {
		await axios
			.delete(`${this.vue.apiRemove}/${id}`)
			.then((response) => {
				if (response.data.code == 500) {
					throw response.data.msg
				}
			})
			.catch((error) => {
				throw error
			})
  }

  extractErrMessage(response) {
    if (response.status == 400) {
      if (Object.prototype.hasOwnProperty.call(response.data, 'message')) {
        throw response.data.message
      } else if (
        Object.prototype.hasOwnProperty.call(response.data.Object, 'title')
      ) {
        throw response.data.title
      }
    } else {
      throw response.statusText
    }
  }
}

export default serviceForm
