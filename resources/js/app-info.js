export default {
  title: 'PT SMM Test API',
  get api () {
    return {
      barang: {
        all: 'api/barang',
        table: 'api/barang/table',
        getById: 'api/barang/detail',
        post: 'api/barang/save',
        put: 'api/barang/update',
        delete: 'api/barang'
      },
      penerimaBarang: {
        all: 'api/penerima-barang',
        table: 'api/penerima-barang/table',
        getById: 'api/penerima-barang/detail',
        post: 'api/penerima-barang/save',
        put: 'api/penerima-barang/update',
        delete: 'api/penerima-barang'
      },
      permintaanBarang: {
        all: 'api/permintaan-barang',
        table: 'api/permintaan-barang/table',
        getById: 'api/permintaan-barang/detail',
        post: 'api/permintaan-barang/save',
        put: 'api/permintaan-barang/update',
        delete: 'api/permintaan-barang'
      }
    }
  }
}
