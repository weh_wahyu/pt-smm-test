import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import appInfo from '~/app-info'
import axios from 'axios'

import '~/plugins/index'
import '~/components/index'

Vue.config.productionTip = false
Vue.prototype.$appInfo = appInfo
Vue.prototype.$axios = axios

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
