<?php

namespace Database\Factories;

use App\Models\PenerimaBarang;
use Illuminate\Database\Eloquent\Factories\Factory;

class PenerimaBarangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PenerimaBarang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nik' => $this->faker->creditCardNumber,
            'name' => $this->faker->name,
            'department' => $this->faker->colorName,
            'description' => $this->faker->text,
            'is_active' => $this->faker->boolean,
        ];
    }
}
