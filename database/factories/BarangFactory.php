<?php

namespace Database\Factories;

use App\Models\Barang;
use Illuminate\Database\Eloquent\Factories\Factory;

class BarangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Barang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'location' => $this->faker->streetAddress,
            'stock' => $this->faker->numberBetween(1, 100),
            'description' => $this->faker->text,
            'is_active' => $this->faker->boolean,
            'status' => $this->faker->text(10),
        ];
    }
}
