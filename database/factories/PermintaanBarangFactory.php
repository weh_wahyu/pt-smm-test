<?php

namespace Database\Factories;

use App\Models\PermintaanBarang;
use Illuminate\Database\Eloquent\Factories\Factory;

class PermintaanBarangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PermintaanBarang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'penerima_barang_id' => $this->faker->numberBetween(1, 10),
            'total_barang' => $this->faker->numberBetween(1, 10),
            'request_date' => $this->faker->dateTime,
            'is_approved' => $this->faker->boolean,
        ];
    }
}
