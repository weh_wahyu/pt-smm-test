<?php

namespace Database\Seeders;

use App\Models\PenerimaBarang;
use Illuminate\Database\Seeder;

class PenerimaBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PenerimaBarang::factory()->count(10)->create();
    }
}
