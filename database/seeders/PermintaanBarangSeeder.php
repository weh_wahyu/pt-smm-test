<?php

namespace Database\Seeders;
use App\Models\PermintaanBarang;
use App\Models\PenerimaBarang;
use App\Models\Barang;
use Illuminate\Database\Seeder;

class PermintaanBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PermintaanBarang::factory()->count(5)->create()->each(function ($item) {
            $penerimaBarang = PenerimaBarang::inRandomOrder()->first();
            $barangList = Barang::inRandomOrder()->limit(2)->get();
            $barangSync = [];
            foreach ($barangList as $key => $barang) {
                $barangSync[$barang->id] = [
                    'stock' => 1,
                ];
            }

            $item->penerimaBarang()->associate($penerimaBarang);
            $item->permintaanBarangItems()->sync($barangSync);
        });
    }
}
