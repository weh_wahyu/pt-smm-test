<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPermintaanBarangItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_barang_items', function (Blueprint $table) {
            $table->foreign(['barang_id'], 'pbi_bid')->references(['id'])->on('barang');
            $table->foreign(['permintaan_barang_id'], 'pbi_pbid')->references(['id'])->on('permintaan_barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_barang_items', function (Blueprint $table) {
            $table->dropForeign('pbi_bid');
            $table->dropForeign('pbi_pbid');
        });
    }
}
