<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenerimaBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penerima_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik', 35);
            $table->string('name');
            $table->string('department', 35)->nullable();
            $table->text('description')->nullable();
            $table->integer('is_active')->nullable()->default(1);
            $table->softDeletes();
            $table->dateTime('created_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerima_barang');
    }
}
